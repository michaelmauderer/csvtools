#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
import os
from csvtools.utils import write_dicts


class NoTrialRunning(Exception):
    pass


class TrialRunning(Exception):
    pass

    
class TrialLogger():
            
    def __init__(self, path):
        self._currentTrialLog = None
        self._trialCount = 0
        self._path = path
        
        previous_file_counter = 0
        format_sring = '{path}-{count}.{ext}'
        parts = path.rsplit('.', 1)
        file_name = ''.join(parts[:-1])
        ext = parts[-1]
        
        while os.path.isfile(self._path):
            previous_file_counter += 1 
            self._path = format_sring.format(path=file_name,
                                             count=previous_file_counter,
                                             ext=ext)
              
        with open(self._path, 'w'):
            pass
         
    @property
    def trial_dict(self):
        return self._currentTrialLog
         
    @property  
    def current_trial(self):
        if self._currentTrialLog is None:
            return None
        else:
            return self._trialCount
         
    def log(self, **kwargs):
        if self._currentTrialLog is None:
            raise NoTrialRunning()
        self._currentTrialLog.update(kwargs)
          
    def start_trial(self):
        if self._currentTrialLog is not None:
            raise TrialRunning()
        self._trialCount+=1
        self._currentTrialLog = dict(trial_id=self._trialCount)
       
    def end_trial(self):
        self.__writeData(self._currentTrialLog)
        self._currentTrialLog = None
        
    def __writeData(self, data):
        with open(self._path, 'a') as outFile:
            outFile.write('TrialStart\n')
            for key, value in data.items():
                outFile.write('{0}: {1}\n'.format(key,value))                
            outFile.write('TrialEnd\n')
            
    @classmethod
    def transform_log_to_csv(cls, log_path, csv_path):
        converter = LogConverter(log_path)
        converter.convert_to_csv(csv_path) 

        
class BlockParsingError(Exception):
    pass


class LogConverter():
    
    trial_start = 'TrialStart'
    trial_end = 'TrialEnd'
    
    def __init__(self, in_file):
        self.in_file = in_file
       
    def convert_to_csv(self, out_file):
        file_data = self.get_data()
        write_dicts(out_file, file_data)
    
    def get_data(self):
        return self._convert(self.in_file)
    
    def _convert(self, file_path):
        trials = []
        with open(file_path) as inFile:
            while True:
                try:
                    trial = self._parse_trial(inFile)
                    trials.append(trial)
                except BlockParsingError:
                    return trials
            
    def _parse_trial(self, file):
        start_message = file.readline().strip()
        if start_message != self.trial_start:
            raise BlockParsingError(start_message)
        data = {'File':file.name}
        while True:
            line = file.readline()
            if line is None: break
            
            line = line.strip()
            if line == self.trial_end:
                return data
            else:
                key, value = line.split(':')
                data[key] = value.strip()
                
       
                