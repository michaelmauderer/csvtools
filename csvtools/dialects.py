#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
import csv

class SimpleTabCSV(csv.Dialect):
    """Description of a simple csv file."""
    
    delimiter = '\t'
    lineterminator = '\n'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    quoting = csv.QUOTE_MINIMAL