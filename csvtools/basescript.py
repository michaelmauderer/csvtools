#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
import os
from csvtools.utils import FileIterator, make_runner, write_dicts
from csvtools.processor import Processor
from csvtools.dialects import SimpleTabCSV


class IllegalPathException(Exception):
    pass

@make_runner
def run_script(inPath, outPath, conditions, aggregators, preprocessors=[],
        emptyValue='', addMissingConditions=False,
        **kwargs):
    
    if not os.path.isdir(outPath):
        with open(outPath, 'w'):
            pass
    
    for file in FileIterator(inPath):
        if not '.gazedata' in file:
            print("Skipping non gazedata file %s" % file)
            continue
        print("Processing file %s" % file)
        data =  Processor(file, conditions, addMissingConditions, dialect=SimpleTabCSV)
        
        for aggregator in aggregators:
            data.add_aggregator(aggregator)
        for preprocessor in preprocessors:
            data.add_lineprocessor(preprocessor)
        
        file_result = data.get_result_conditions()  
        
        if os.path.isdir(outPath):
            outFile = os.path.join(outPath, os.path.basename(file))
            write_dicts(outFile, file_result, restval=emptyValue)
        else:
            write_dicts(outPath, file_result, restval=emptyValue, mode='a')
        
        print("Writing results to %s" % outPath)
        
                
    print("All files processed.")
    print("---")
    print("Finished.")