#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
import os
import re
import csv
from .dialects import SimpleTabCSV
import inspect
import logging

    
class FileIterator():
    """
    A File Iterator allows for easy iteration over all files in a given folder.
    """
    
    def __init__(self, path, regex = None):
        """
        Create a FileIterator.
        A file iterator is an iterable that will return all files in the given 
        folder (in no specific order).
        Additional information can be extracted from the filenames if a regular
        expression containing named subgroups is passed to the constructor.
        If such a expression has been used upon construction of the FileIterator
        it will on each iteration not only return the absolute file path but
        also a dictionary containing the results for the regular expression.
        
        
        Arguments:
        folder -- All files in the given folder will be iterated over.
        
        Optional Arguments:
        regex -- A regular expression containing named subgroups can be used
        to extract information from the file names.
        
        """
        self.path = path
        self.regex = regex
       
    def __iter__(self):

        if os.path.isdir(self.path):
            files = os.listdir(self.path)
        else:
            files = [self.path]
        
        files = list(filter(lambda s: not s.startswith('.'), files))
        r = re.compile(self.regex) if self.regex is not None else None
        
        while files:
            file = files.pop()
            path = self._make_absolute(self.path,file)
            if r is None:
                yield path
            else:          
                match = r.match(file)
                if match:
                    yield  path, r.match(file).groupdict()
                else:
                    yield path, {}
                    
    def _make_absolute(self, path, file):
        return os.path.abspath(os.path.join(path , file))
    
    
def write_dicts(path, dicts, dialect=SimpleTabCSV, sort=sorted, restval='',
                mode='w'):
    """
    Writes all dictionaries in a CSV file to the given location.
    
    Every key that appears in any one of the dictionaries will be used as a header.
    Headers will either be sorted alphabetically or by the given sort function.
    """
    header = set()
    for d in dicts:
        header.update(d.keys())
    header = sort(list(header))
    with open(path, mode=mode) as out:
        writer = csv.DictWriter(out, header, dialect=dialect, restval=restval)
        writer.writeheader()
        writer.writerows(dicts)
        
        
class __Empty():
    pass
    
def make_runner(fun, log_file='./error.log'):
    '''
    Decorator for functions that should give easy understandable error messages 
    for missing parameters and log all unhandled exceptions in a error log.
    '''
    argspec =  inspect.getargspec(fun)
    argLen = len(argspec.args) if argspec.args is not None else 0
    defaultLen = len(argspec.defaults) if argspec.defaults is not None else 0
    defaults = [__Empty] * (argLen - defaultLen) + (list(argspec.defaults) if argspec.defaults is not None else [])

    def runner(**kwargs):
        handler = logging.FileHandler(log_file)
        log = logging.getLogger('Runner')
        log.addHandler(handler) 

        #Check that all needed arguments are supplied.
        missing = []
        for var, default in zip(argspec.args, defaults):
            if not var in kwargs and default is __Empty:
                missing.append(var)
                
        if missing:
            if len(missing) > 1:
                print("Please supply values for these variables: ",", ".join(missing))
            else:
                print("Please supply a value for this variable:", missing[0])    
            exit()
        
        try:
            return fun(**kwargs)
        except:
            log.exception('Exception:')
            raise
            
    return runner
    