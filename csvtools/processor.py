#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
import csv
import itertools


class Processor():
    '''
    The Processor is a wrapper for the the csv file that should be aggregated.
    '''

    def __init__(self, path, conditon_columns, add_missing_conditions=False,
                 constant_columns=[], dialect=csv.excel):
        '''
        Create a Processor instance.
        
        Arguments:
        path -- absolute path to the data file.
        condition_columns -- list of columns that are used to specify a condition.
        
        Optional Arguments:
        constant_columns -- columns that should stay the same for all
                            lines (in a specific condition).
        add_missing_conditions -- flag that indicates whether empty conditions
                                  should be guessed and added to the result.
        dialect -- the csv dialect used in the input file.
        '''
        
        self.__path = path
        self.__dialect = dialect
        self.__aggregators = []
        self.__lineprocessors = []
        self.__constant_columns = constant_columns
        self.condition_columns = conditon_columns
        self.add_missing_conditions = add_missing_conditions
        self.__result_data = None
        self.__encountered_conditions = []
        
        self.__constant_aggregators = []
        for fieldName in self.__constant_columns:
            aggregator = ConstantFieldAggregator(fieldName)
            self.__constant_aggregators.append(aggregator)
            
     
    def add_lineprocessor(self, preprocessor):
        self.__lineprocessors.append(preprocessor)    
            
    def add_aggregator(self, aggregator):
        self.__aggregators.append(aggregator)
    
    def __get_condition(self, lineData): 
        return tuple([lineData[cField] for cField in self.condition_columns ])
    
    def __aggregate(self):
        '''
        Parse the file and provide all registered aggregators with data.
        '''
        self.__result_data = list()
        last_condition = None
        with open(self.__path) as inFile:
            reader = csv.DictReader(inFile, dialect=self.__dialect)
            for line in reader:
                for lineprocessor in self.__lineprocessors:
                        lineprocessor.process_line(line)
                
                if not line:
                    continue
                        
                condition = self.__get_condition(line)
                newBlock = False
                if last_condition != condition:
                    newBlock = True
                    last_condition = condition   
                    if condition not in self.__encountered_conditions:
                        self.__encountered_conditions.append(condition)
                    
                for aggregator in self.__aggregators:
                    aggregator.aggregate_line(line, condition, newBlock) 
    
                for aggregator in self.__constant_aggregators:
                    aggregator.aggregate_line(line, self, newBlock) 
    
        for condition in self.__encountered_conditions:
            result = dict(zip(self.condition_columns, condition))
            for aggregator in self.__aggregators:
                aggResult = aggregator.get_result(condition)
                result.update(aggResult)
            self.__result_data.append(result)
        
        if self.add_missing_conditions:
            for item in self._getCompleteExtendedConditions(self.__encountered_conditions):
                if item not in self.__encountered_conditions:
                    self.__result_data.append(dict(zip(self.condition_columns, condition)))
                
    def _getCompleteExtendedConditions(self, conditions):
        conditionSets = [set() for _ in range(len(self.condition_columns))]
        for condition in conditions:
            for value, cSet in zip(condition, conditionSets):
                cSet.add(value)
        return itertools.product(*conditionSets)
            
    def get_result_conditions(self):
        '''
        Return result as a list of dictionaries.
        Each dictionary represents one condition and contains the condition fields
        with their respective values and the aggregated values as returned from
        the aggregators.
        '''
        if self.__result_data is None:
            self.__aggregate() 
        return self.__result_data
    
    def get_result_compact(self):
        '''
        Return result as a dictionary.
        The dictionary contains fields for all conditions and aggregator values.
        The field names are combinations of the condition values and the 
        names the aggregators provide e.g. '<subject>_<set_size>_RT_Mean'.
        '''
        self.get_result_conditions()
        resultLine = dict()
        for condition in self.__encountered_conditions:
            condition_string = '_'.join(condition)
            for aggregator in self.__aggregators:
                aggResult = aggregator.get_result(condition)
                for key, value in aggResult.items():
                    resultLine['{}_{}'.format(condition_string,key)] = value
            for aggregator in self.__constant_aggregators:
                resultLine.update(aggregator.get_result(self))
        return resultLine
            

class BaseLineProcessor():
    '''
    Base class for all LineProcessors.
    '''
    
    def process_line(self, line_data):
        '''
        Modify the given line_data in-place for the desired result.
        Overwrite this function in you subclass.
        '''
        pass    
    
    
class FunctionLineProcessor(BaseLineProcessor):
    '''
    Wrapper for a arbitrary function that will be used on the line_data.
    '''
    
    def __init__(self, function):
        self.__function = function
    
    def process_line(self, line_data):
        self.__function(line_data)


class NullValueReplacementProcessor(BaseLineProcessor):
    '''
    The NullValueReplacementProcessor will replace specified values in certain
    columns with None. These will then be ignored for further processing.
    '''
    
    def __init__(self, null_values_for_fields):
        '''
        Arguments:
        null_values_for_fields -- dictionary mapping from field names to a iterable
                               of possible null values.
        '''
        self._nullValuesForFields = null_values_for_fields
    
    def process_line(self, line_data):
        for key in self._nullValuesForFields:
            if line_data[key] in self._nullValuesForFields[key] or line_data[key].strip == '':
                line_data[key] = None
        
    
class BaseAggregator():
    '''
    Aggregators are the base units for will gathering and evaluating data from 
    a csv-file. 
    A BaseAggregator's aggregate_line function will be called while
    iterating over the csv-file and receive all the data contained as a dictionary
    (as given by a csv.DictReader) and thze current condition for further
    processing.
    
    For subclassing you might consider overwriting _initialise condition, 
    _process_data and _make result. For further information look at the 
    respective doc-string.
    '''
    
    def __init__(self):
        self._result_dict = dict()
        
    def aggregate_line(self, data, condition, new_block=False):
        '''
        Handles the data contained in the current line for the given condition.
        '''
        #Look whether we already have data for the current condition.
        if condition not in self._result_dict:
            #If not initialise the condition.
            self._result_dict[condition] = self._initialise_condition()
        #Get stored data for the current condition and process it together
        #with the new data.
        current_data = self._result_dict[condition]    
        new_data = self._process_data(data, current_data, new_block)
        #Save the processed data for the current condition.
        self._result_dict[condition] = new_data
            
    def _initialise_condition(self):
        '''
        Return the data for an empty condition.
        '''
        return None
    
    def _process_data(self, new_data, current_data, new_block):
        '''
        Process the received data and return the result to be stored.
        
        Arguments:
        new_data -- The current line data as a dictionary.
        current_data -- The currently saved data for a condition.
        new_block -- Indicates whether the previous line was of the same condition.
        '''
        return None
    
    def _make_result(self, current_data):
        '''
        Process the stored data for a condition and return the final value.
        
        Arguments:
        current_data -- The data stored for a condition. 
        '''
        return current_data
        
    def get_result(self, condition=None):
        '''
        Return the result of the given condition.
        
        The result will be a dictionary containing heading name(s) and the 
        according result value(s).
        Overwrite this method if you want to return more than one value.
        '''
        result = self._make_result(self._result_dict.get(condition))
        return {self._getResultFieldName() : result }
    
    def _getResultFieldName(self):
        '''
        Return the name of the heading for the end result.
        Convenience method for results that only return one value. Will be 
        called by get_result if it has not been overwritten.
        '''
        return 'AggregatedData'
            
                        
class ColumnAggregator(BaseAggregator):
    '''
    The ColumnAggregator is a convenience class for aggregators that only use 
    one column to determine their result.
    
    It will extract the value for the given column and process it. If the value
    is None it will be ignored. This value will then be passed to _aggregate_colum.
    This function works otherwise like _process_data.
    
    '''
    
    def __init__(self, column_name):
        '''
        Arguments:
        column_name -- Name of the column that should be aggregated.
        '''
        BaseAggregator.__init__(self)
        self._column_name = column_name
        
    @property
    def column_name(self):
        return self._column_name
    
    def _process_data(self, new_data, current_data, new_block):
        if (new_data[self._column_name] is None) or (new_data[self._column_name].strip() == ''):
            return current_data
        return self._aggregate_column(new_data[self.column_name], current_data, new_block=new_block)
      
    def _aggregate_column(self, new_data, current_data, new_block):
        '''
        Process the received data and return the result to be stored.
        
        Arguments:
        new_data -- Current value of the column.
        current_data -- The currently saved data for a condition.
        new_block -- Indicates whether the previous line was of the same condition.
        '''
        
        pass
    
    def _getResultFieldName(self):
        return self._column_name + '_' + 'AggregationData' 
   
    
class ChangedValueException(Exception):
    def __init__(self, correct, received, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

    
class ConstantFieldAggregator(ColumnAggregator):
    '''
    The ConstantFieldAggregator will assume the first value found in the
    given column for each condition.
    Important: The value MUST stay the same for the condition otherwise a 
    ChangedValueException will be thrown.
    The result will be saved in the column named <FieldName>.
    '''
    def _initialise_condition(self):
        return None
    
    def _aggregate_column(self, new_data, current_data, new_block):
        if current_data is None:
            return new_data
        elif current_data is not None and new_data != current_data:
            raise ChangedValueException(current_data, new_data)
    
    def _getResultFieldName(self, condition=None):
        return self.column_name 
    
        
class SummationAggregator(ColumnAggregator):
    '''
    The SummationAggregator will sum up all valid values in the given column.
    The results will be save in a new column named <FieldName>_Sum.
    '''
    
    def _initialise_condition(self):
        return 0
    
    def _aggregate_column(self, new_data, current_data, new_block):
        return float(new_data) + current_data
        
    def _getResultFieldName(self, condition=None):
        return self.column_name + '_' + 'Sum' 
    
    
class MeanAggregator(ColumnAggregator):
    '''
    The MeanAggregator will calculate the mean of all valid values in the
    given column.
    The result will be save in a new column named <FieldName>_Mean.
    '''
    
    def _initialise_condition(self):
        return 0,0
    
    def _aggregate_column(self, new_data, current_data, new_block):
        overall_sum, count = current_data
        return overall_sum+float(new_data), count+1
        
    def _make_result(self, current_data):
        overall_sum, count = current_data
        return overall_sum/count
    
    def _getResultFieldName(self, condition=None):
        return self.column_name + '_' + 'Mean' 
   
          
class CountAggregator(ColumnAggregator):
    '''
    The CountAggregator will create a count for all valid data values in the 
    given column.
    The results will be saved in a column named <FieldName>_Count.
    '''
    
    def _initialise_condition(self):
        return 0
    
    def _aggregate_column(self, new_data, current_data, new_block):
        return current_data+1
           
    def _getResultFieldName(self, condition=None):
        return self.column_name + '_' + 'Count' 
    
    
class SwitchCountAggregator(ColumnAggregator):
    '''
    The SwitchCountAggregator will count the number of value changes in the given 
    column. 
    The result will be saved to a column named <FieldName>_SwitchCount.
    
    IMPORTANT: Since each condition is considered individually, changes that happen
    between conditions may not be counted.
    e.g.
    Block | 1   | 2     | 1   | 2
    Item  | Red | Green | Red | Green
    In this example no changes would be found if Block is a condition!
    '''   
    def _initialise_condition(self):
        return -1, None
    
    def _aggregate_column(self, new_data, current_data, new_block):
        count, lastItem = current_data
        if lastItem != new_data:
            count+=1
            lastItem = new_data       
        return count, lastItem
    
    def _make_result(self, current_data):
        count, _ = current_data
        return count
  
    def _getResultFieldName(self, condition=None):
        return self.column_name + '_' + 'SwitchCount' 
   
   
class TimeIntervalMeanAggregator(BaseAggregator):
    '''
    The TimeIntervalMeanAggregator will create means for all valid values in 
    the given column. The means will be split into time intervals of the given
    length, starting at the first found timestamp.
    For each interval result will be saved in a column named
    <FieldName>_MeanInterval_<IntervallNumber>.
    The last unfinished interval will be droped.
    '''
    
    def __init__(self, column_name, interval, time_column):
        BaseAggregator.__init__(self)
        self._column_name = column_name
        self._interval = interval
        self._time_column = time_column
    
    def _initialise_condition(self):
        return  0, 0, None, []
    
    def _process_data(self, new_data, current_data, new_block):
        current_sum, count, intervall_start_time, history = current_data
        now = float(new_data[self._time_column])
        if intervall_start_time is None:
            intervall_start_time = now
        agg_value = float(new_data[self._column_name])
        
        if now - intervall_start_time < self._interval:
            current_sum += agg_value
            count+=1
        else:
            history.append(current_sum/count)
            intervall_start_time = now
            current_sum = agg_value
            count = 1
        return current_sum, count, intervall_start_time, history
           
    def get_result(self, condition=None):
        value, count, intervallStartTime, history = self._result_dict[condition]
        result = dict()
        namePattern = self._column_name +'_MeanInterval_{}'
        for index, value in enumerate(history):
            result[namePattern.format(index)] = value
        return result
    
    
class ObjectCountAggregator(ColumnAggregator):
    '''
    The ObjectCountAggregator will create a count for all found values in the
    given column.
    For each counted value the result will be saved in a column named <Value>_Count.
    '''
    
    def _initialise_condition(self):
        return dict()
    
    def _aggregate_column(self, new_data, current_data, new_block):
        count = current_data.get(new_data,0)
        current_data[new_data] = count+1
        return current_data
           
    def get_result(self, condition):
        counts = self._result_dict.get(condition, dict())
        result = dict()
        for obj, count in counts.items():
            result['{}_Count'.format(obj)] = count
        return result
