#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
from setuptools import setup, find_packages
setup(
    name = "csvtools",
    version = "0.1",
    packages = find_packages(),
    
    author = "Michael Mauderer",
    author_email = "mail@michaelmauderer.de",
)