.. csvtools documentation master file, created by
   sphinx-quickstart on Tue Jul 31 22:08:48 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to csvtools's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2
   
.. automodule:: csvtools.processor
   :members:
   
.. automodule:: csvtools.dialects
   :members:
   
.. automodule:: csvtools.utils
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

