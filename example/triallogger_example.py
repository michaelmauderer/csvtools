#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
import csvtools

logger = csvtools.TrialLogger('./log.log')

for n in range(10):
    logger.start_trial()
    
    logger.log(RT=0.1)
    logger.log(RESP='c')
    logger.log(CRESP='a')
    for n in range(10):
        logger.trial_dict['CostumKey{0}'.format(n)] = 'Value({0})'.format(n)
    
    
    logger.end_trial()
    
csvtools.TrialLogger.transform_log_to_csv('./log.log', './log.csv')


