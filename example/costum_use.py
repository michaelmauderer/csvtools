#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
from csvtools import *


class MyPreprocessor(BaseLineProcessor):
    '''
    Custom LinePreprocessor that will create a new column.
    '''
    
    def process_line(self, line_data):
        if line_data['RESP'] == line_data['CRESP']:
            line_data['ACC'] = '1'
        else:
            line_data['ACC'] = '0'
            
#Set up the the Processor            
p = Processor('./exampleData/example1.csv', ['ACC'], dialect=SimpleTabCSV)
p.add_lineprocessor(MyPreprocessor())
p.add_aggregator(MeanAggregator('RT'))

#Return the result in different formats.
print(p.get_result_compact())
print('---------')
print(p.get_result_conditions())

#Write the result directly to a file.
#write_dicts('./out',p.get_result_conditions())