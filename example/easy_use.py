#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
from csvtools import *

## Path to the file or folder containing the data.
## ./ indicates a relative path to the script location.
## e.g. ./in means: take the folder 'in' that is located in the same folder
## as the script.
inPath = './exampleData/in'


## Path to the file or folder that should be used to write the output files.
## Works like inPath.
outPath = "./exampleData/out"


## conditions should be set to a list of strings that indicate the column names
## of the conditions that should be used for aggregation.
## For every combinations of values(!) in these columns one condition will be
## created.
## e.g. ['Subject', 'CurrentObject' ] might create the conditions (1, 'Left'), (1, 'Right'),
## (2, 'Left') and (2, 'Right').
conditions = ['Subject', 'Block' ]


## The list of aggregators determines which columns should be aggregated in
## which way. Each aggregator will create one or more output values, that depend
## on his input and set options.
## It is important to note, that the input values for each condition are 
## considered to be completely independent from each other! 
##
## Currently the following aggregators are available:
##
## ==ConstantFieldAggregator==
## The ConstantFieldAggregator will assume the first value found in the
## given column for each condition.
## Important: The value MUST stay the same for the condition otherwise a 
## ChangedValueException will be thrown.
##The result will be saved in the column named <FieldName>
##
## ==CountAggregator==
## The CountAggregator will create a count for all valid data values in the
## given column.
## The results will be saved in a column named <FieldName>_Count.
##
## ==MeanAggregator==
## The MeanAggregator will calculate the mean of all valid values in the
## given column.
## The result will be save in a new column named <FieldName>_Mean.
##
## ==ObjectCountAggregator==
## The ObjectCountAggregator will create a count for all found values in the
## given column.
## For each counted value the result will be saved in a column named <Value>_Count
##
## ==SummationAggregator==
## The SummationAggregator will sum up all valid values in the given column.
## The results will be save in a new column named <FieldName>_Sum.
##
## ==SwitchCountAggregator==
## The SwitchCountAggregator will count the number of value changes in the given 
## column. 
## The result will be saved to a column named <FieldName>_SwitchCount.
## 
## IMPORTANT: Since each condition is considered individually, changes that happen
## between conditions may not be counted.
## e.g.
## Block | 1   | 2     | 1      | 2
## Item  | Red | Green | Yellow | Green
## In this example no changes would be found for Block 2!
##
## ==TimeIntervallMeanAggregator==
## The TimeIntervallMeanAggregator will create means for all valid values in 
## the given column. The means will be split into time intervals of the given
## length (in ms), starting at the first found timestamp.
## For each interval result will be saved in a column named 
## <FieldName>_MeanIntervall_<IntervallNumber>
##
aggregators = [SummationAggregator('DiameterPupilRightEye'),
               CountAggregator('DiameterPupilRightEye'),
               MeanAggregator('DiameterPupilRightEye'),
               SwitchCountAggregator('CurrentObject'),
               TimeIntervalMeanAggregator('DiameterPupilRightEye', 9, 'TETTime'),
               ObjectCountAggregator('CurrentObject'),
               ConstantFieldAggregator('Session'),
               ]


#DO NOT CHANGE THIS LINE!
run_script(**locals())