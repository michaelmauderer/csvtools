#*******************************************************************************
#
# Developer(s):
# * Michael Mauderer <mail@MichaelMauderer.de>
#
#*******************************************************************************
import unittest
from csvtools.processor import Processor, SummationAggregator, MeanAggregator,\
    CountAggregator, ObjectCountAggregator, SwitchCountAggregator,\
    TimeIntervalMeanAggregator
from csvtools.dialects import SimpleTabCSV


class SubjectBasedTest(unittest.TestCase):
    
    data_path = './fixtures/testData1.csv'
    conditon_columns = ['Subject']
    
    def setUp(self):
        self.processor = Processor(self.data_path, self.conditon_columns, dialect=SimpleTabCSV)


class BlockBasedTest(SubjectBasedTest):
    
    conditon_columns =  ['Subject', 'Block']


class TestSumSimple(SubjectBasedTest):
   
    def test_sum_left(self):
        result = 60.1002760174
        self.processor.add_aggregator(SummationAggregator('DiameterPupilLeftEye'))
        self.assertAlmostEqual(self.processor.get_result_conditions()[0]['DiameterPupilLeftEye_Sum'], result, 5)

    def test_sum_right(self):
        result = 62.36673241410805
        self.processor.add_aggregator(SummationAggregator('DiameterPupilRightEye'))
        self.assertAlmostEqual(self.processor.get_result_conditions()[0]['DiameterPupilRightEye_Sum'], result, 5)


class TestSumAdvanced(BlockBasedTest):
    
    def test_sum_left(self):
        
        result0 = 20.955368033319107
        result1 = 22.106621721286988
        result2 = 17.038286262768466
        
        self.processor.add_aggregator(SummationAggregator('DiameterPupilLeftEye'))
        processor_result = self.processor.get_result_conditions()

        self.assertAlmostEqual(processor_result[0]['DiameterPupilLeftEye_Sum'], result0, 5)
        self.assertAlmostEqual(processor_result[1]['DiameterPupilLeftEye_Sum'], result1, 5)
        self.assertAlmostEqual(processor_result[2]['DiameterPupilLeftEye_Sum'], result2, 5)
        
        
class TestMeanSimple(SubjectBasedTest):
    
    def test_mean_left(self):
        self.processor.add_aggregator(MeanAggregator('DiameterPupilLeftEye'))
        processor_result = self.processor.get_result_conditions()
        c_result =  2.0033425339124853
        self.assertAlmostEqual(processor_result[0]['DiameterPupilLeftEye_Mean'], c_result, 5)
        

class TestObjectCountSimple(SubjectBasedTest):
    
    def test_object_count(self):
        self.processor.add_aggregator(ObjectCountAggregator('CurrentObject'))
        processor_result = self.processor.get_result_conditions()
        c_result_r =  18
        c_result_l =  5
        self.assertEqual(processor_result[0]['L_Count'], c_result_l)   
        self.assertEqual(processor_result[0]['R_Count'], c_result_r)        
        
        
class TestCountSimple(SubjectBasedTest):
    
    def test_count(self):
        self.processor.add_aggregator(CountAggregator('CurrentObject'))
        processor_result = self.processor.get_result_conditions()
        c_result= 23
        self.assertEqual(processor_result[0]['CurrentObject_Count'], c_result)   
        
        
class TestSwitchCountSimple(SubjectBasedTest):
    
    def test_switchcount(self):
        self.processor.add_aggregator(SwitchCountAggregator('CurrentObject'))
        processor_result = self.processor.get_result_conditions()
        c_result= 9
        self.assertEqual(processor_result[0]['CurrentObject_SwitchCount'], c_result)   
        
        
class TestIntervalMean(SubjectBasedTest):
    
    data_path = './fixtures/intervalTestData.csv'
    
    def test_interval_mean(self):
        self.processor.add_aggregator(TimeIntervalMeanAggregator('Value', 2, 'Time'))
        processor_result = self.processor.get_result_conditions()
        c_result= 1.5
        for key in processor_result[0]:
            if key.startswith('Value_MeanInterval'):
                self.assertEqual(processor_result[0][key], c_result)
        